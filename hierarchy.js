var hierarchy =
[
    [ "AppDelegate()", "category_app_delegate_07_08.html", null ],
    [ "<CBCentralManagerDelegate>", null, [
      [ "DDSBTLECentralManager()", "category_d_d_s_b_t_l_e_central_manager_07_08.html", null ]
    ] ],
    [ "<CBPeripheralDelegate>", null, [
      [ "DDSBTLECentralManager()", "category_d_d_s_b_t_l_e_central_manager_07_08.html", null ]
    ] ],
    [ "NSObject", null, [
      [ "DDSBTLECentralManager", "interface_d_d_s_b_t_l_e_central_manager.html", null ],
      [ "DDSeWeigh", "interface_d_d_se_weigh.html", null ]
    ] ],
    [ "<NSObject>", null, [
      [ "<eWeighDataSourceDelegate>", "protocole_weigh_data_source_delegate-p.html", [
        [ "DDSeWeigh", "interface_d_d_se_weigh.html", null ]
      ] ]
    ] ],
    [ "<UIApplicationDelegate>", null, [
      [ "AppDelegate", "interface_app_delegate.html", null ]
    ] ],
    [ "UIResponder", null, [
      [ "AppDelegate", "interface_app_delegate.html", null ]
    ] ],
    [ "UIViewController", null, [
      [ "ViewController", "interface_view_controller.html", null ]
    ] ],
    [ "ViewController()", "category_view_controller_07_08.html", null ],
    [ "XCTestCase", null, [
      [ "eWeigh_Sample_AppTests", "interfacee_weigh___sample___app_tests.html", null ]
    ] ]
];