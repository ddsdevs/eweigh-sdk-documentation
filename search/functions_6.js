var searchData=
[
  ['scan4eweigh_59',['scan4eWeigh',['../interface_d_d_se_weigh.html#a0f1d7301c55b92ee841ebdfcbce5013f',1,'DDSeWeigh']]],
  ['scan4eweighstop_60',['scan4eWeighStop',['../interface_d_d_se_weigh.html#a58b384b67c06b4fd2ae0059095d855e1',1,'DDSeWeigh']]],
  ['scanwithtimeout_3a_61',['scanWithTimeout:',['../interface_d_d_s_b_t_l_e_central_manager.html#ae87cef19b9ce6898fae7c9fbeef6cec4',1,'DDSBTLECentralManager']]],
  ['setalgorithm_3a_62',['setAlgorithm:',['../interface_d_d_se_weigh.html#a188641110946801707f89d6e88b62d70',1,'DDSeWeigh']]],
  ['setscaleforbartype_3a_63',['setScaleForBartype:',['../interface_d_d_se_weigh.html#a72a1e31365fe5bb1a396febbb278a36f',1,'DDSeWeigh']]],
  ['sharedcomms_64',['sharedComms',['../interface_d_d_se_weigh.html#acaca264b0897a438368a0ab70df4bcde',1,'DDSeWeigh']]],
  ['stopsampling_65',['stopSampling',['../interface_d_d_se_weigh.html#a3ba034c3aa5f8991c20eb82483836739',1,'DDSeWeigh']]]
];
