var searchData=
[
  ['scan4eweigh_22',['scan4eWeigh',['../interface_d_d_se_weigh.html#a0f1d7301c55b92ee841ebdfcbce5013f',1,'DDSeWeigh']]],
  ['scan4eweighstop_23',['scan4eWeighStop',['../interface_d_d_se_weigh.html#a58b384b67c06b4fd2ae0059095d855e1',1,'DDSeWeigh']]],
  ['scanwithtimeout_3a_24',['scanWithTimeout:',['../interface_d_d_s_b_t_l_e_central_manager.html#ae87cef19b9ce6898fae7c9fbeef6cec4',1,'DDSBTLECentralManager']]],
  ['setalgorithm_3a_25',['setAlgorithm:',['../interface_d_d_se_weigh.html#a188641110946801707f89d6e88b62d70',1,'DDSeWeigh']]],
  ['setscaleforbartype_3a_26',['setScaleForBartype:',['../interface_d_d_se_weigh.html#a72a1e31365fe5bb1a396febbb278a36f',1,'DDSeWeigh']]],
  ['sharedcomms_27',['sharedComms',['../interface_d_d_se_weigh.html#acaca264b0897a438368a0ab70df4bcde',1,'DDSeWeigh']]],
  ['span_28',['span',['../interface_d_d_se_weigh.html#a0f763dd28ef45b937991afc533cbc8f4',1,'DDSeWeigh']]],
  ['spanpercentage_29',['spanPercentage',['../interface_d_d_se_weigh.html#a4cc63d1792b87152a46ca64d9bd9d269',1,'DDSeWeigh']]],
  ['species_30',['species',['../interface_d_d_se_weigh.html#a367d4c4d5136898613314c915aa3af0f',1,'DDSeWeigh']]],
  ['spotweight_31',['spotWeight',['../interface_d_d_se_weigh.html#ae38717a2d5223263181377b6a6b7d0cd',1,'DDSeWeigh']]],
  ['stableweight_32',['stableWeight',['../interface_d_d_se_weigh.html#aa19ab29655d437b2548090d516be49fa',1,'DDSeWeigh']]],
  ['stableweightasdouble_33',['stableWeightAsDouble',['../interface_d_d_se_weigh.html#a4227beb3534c15a6603312ac975f96a7',1,'DDSeWeigh']]],
  ['stopsampling_34',['stopSampling',['../interface_d_d_se_weigh.html#a3ba034c3aa5f8991c20eb82483836739',1,'DDSeWeigh']]]
];
